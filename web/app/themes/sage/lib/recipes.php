<?php

use Roots\Sage\config;
/**
**/




   // Post type: Recipes
add_action('init', function(){

    $labels = array(
        'name'                  => __( 'Recipes',               __NAMESPACE__ ),
        'singular_name'         => __( 'Recipe',                __NAMESPACE__ ),
        'menu_name'             => __( 'Recipes',               __NAMESPACE__ ),
        'all_items'             => __( 'All Recipes',           __NAMESPACE__ ),
        'add_new'               => __( 'Add New',                    __NAMESPACE__ ),
        'add_new_item'          => __( 'Add New Recipe',        __NAMESPACE__ ),
        'edit_item'             => __( 'Edit Recipe',           __NAMESPACE__ ),
        'new_item'              => __( 'New Recipe',            __NAMESPACE__ ),
        'view_item'             => __( 'View SRecipe',          __NAMESPACE__ ),
        'search_items'          => __( 'Search Recipes',        __NAMESPACE__ ),
        'not_found'             => __( 'No item found',              __NAMESPACE__ ),
        'not_found_in_trash'    => __( 'No item found in Trash',     __NAMESPACE__ )
        );

    $args = array(
        'labels'                => $labels,
        'public'                => true,
          'has_archive'           => true,
              'rewrite' => array(
           'slug'=>'Recipes',
           'with_front'=> false,
           'feed'=> true,
           'pages'=> true),
        'exclude_from_search'   => false,
        'menu_icon'             => 'dashicons-clipboard',
        'capability_type'       => 'post',
        'supports'              => array('title', 'thumbnail', 'revisions')
        );
    register_post_type('shirley_recipes', $args);


    flush_rewrite_rules();

register_taxonomy_for_object_type( 'category', 'shirley_recipes' );
    //categorias para os sliders
register_taxonomy('recipe_group',
array('shirley_recipes'),
array(
    'label'                     => __('Recipe Groups',              __NAMESPACE__),
    'hierarchical'              => true,
    'singular_label'            => __('Recipe Group',    __NAMESPACE__),
    'rewrite'                   => true
    )
);

function recipe_group() {
register_taxonomy(
    'recipe_group',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
    'shirley_recipes',        //post type name
    array(
        'hierarchical' => true,
        'label' => 'Recipe group',  //Display name
        'query_var' => true,

    )
);
}
add_action( 'init', 'recipe_group');
});




$prefix = 'recipe';



$fields_headlines= array(

    array(
        'label'=> 'Subtitle',
        'desc'  => 'Recipe Headline',
        'id'    => $prefix.'headline',
        'type'  => 'text'
    ),


    );



$fields_teaser = array(

    array(
        'label'=> 'Teaser',
        'desc'  => 'Small description',
        'id'    => $prefix.'teaser',
        'type'  => 'editor',
                 'sanitizer' => 'wp_kses_post',
    'settings' => array(
        'textarea_name' =>  $prefix.'teaser'
    )
    )
    );
$fields_intro = array(

    array(
        'label'=> 'Introduction',
        'desc'  => 'Introduction',
        'id'    => $prefix.'intro',
        'type'  => 'editor',
                 'sanitizer' => 'wp_kses_post',
    'settings' => array(
        'textarea_name' =>  $prefix.'intro'
    )
    )
    );




$fields_facts= array(

    array(
        'label'=> 'Preparation Time',
        'desc'  => 'The length of time it takes to prepare the recipe for dish',
        'id'    => $prefix.'prepTime',
        'type'  => 'text'
    ),

   array(
        'label'=> 'Cook Time',
        'desc'  => 'The time it takes to actually cook the dish',
        'id'    => $prefix.'cookTime',
        'type'  => 'text'
    ),

     array(
        'label'=> 'Total Time',
        'desc'  => 'The total time it takes to prepare and cook the dish,',
        'id'    => $prefix.'totalTime',
        'type'  => 'text'
    ),
   array(
        'label'=> 'temperatura horno',
        'desc'  => '',
        'id'    => $prefix.'temphorno',
        'type'  => 'text'
    ),
  array(
    'label' => 'Difilculty Level',
    'desc'  => '',
    'id'    => $prefix . 'dificulty',
    'type'  => 'select',
    'options' => array (
            'one' => array (
                'label' => 'Super Easy',
                'value' => 'supereasy'
            ),

            'three' => array (
                'label' => 'Intermediate',
                'value' => 'intermediate'
            ),

            'five' => array (
                'label' => 'Expert Level',
                'value' => 'expert'
            ),

        )
    ),
     array(
        'label'=> 'Recipe Yield',
        'desc'  => 'The quantity produced by the recipe. For example: number of people served, or number of servings',
        'id'    => $prefix.'recipeYield',
        'type'  => 'text'
    ),



    );

$fields_nutrition= array(

    array(
        'label'=> 'Serving Size',
        'desc'  => 'The serving size, in terms of the number of volume or mass.',
        'id'    => $prefix.'servingSize',
        'type'  => 'text'
    ),

   array(
        'label'=> 'Calories',
        'desc'  => 'The number of calories.',
        'id'    => $prefix.'calories',
        'type'  => 'text'
    ),

     array(
        'label'=> 'Fat Content',
        'desc'  => 'The number of grams of fat.',
        'id'    => $prefix.'fatContent',
        'type'  => 'text'
    ),
     array(
        'label'=> 'Protein Content',
        'desc'  => 'The number of grams of protein.',
        'id'    => $prefix.'proteinContent',
        'type'  => 'text'
    ),
          array(
        'label'=> 'Sugar Content',
        'desc'  => 'The number of grams of sugar.',
        'id'    => $prefix.'sugarContent',
        'type'  => 'text'
    ),
                    array(
        'label'=> 'Trans Fat Content',
        'desc'  => 'The number of grams of trans fat.',
        'id'    => $prefix.'transFatContent',
        'type'  => 'text'
    )

    );

$fields_ingredients = array( array(
    'label' => 'Add Ingredients ',
    'desc'  => 'Ingredients for this recipe.',
    'id'    => $prefix . 'repeatable',
    'type'  => 'repeatable',
    'sanitizer' => array(

        'title' => 'sanitize_text_field',
        'quantidade' => 'sanitize_text_field'
    ),
    'repeatable_fields' => array(

        'title' => array(
            'label' => 'Ingredient',
            'id' => 'ingredient',
            'type' => 'text'
        ),
         'quantidade' => array(
            'label' => 'Cantidad',
            'id' => 'cantidad',
            'type' => 'text'
        )
    )
),
);
$fields_cooking = array(

    array(
        'label'=> 'Recipe Instructions',
        'desc'  => '',
        'id'    => $prefix.'recipeInstructions',
        'type'  => 'editor',
                 'sanitizer' => 'wp_kses_post',
    'settings' => array(
        'textarea_name' =>  $prefix.'recipeInstructions'
    )
    )
    );
$fields_notas = array(

    array(
        'label'=> '',
        'desc'  => '',
        'id'    => $prefix.'notas',
        'type'  => 'editor',
                 'sanitizer' => 'wp_kses_post',
    'settings' => array(
        'textarea_name' =>  $prefix.'notas'
    )
    )
    );
$fields_ref = array(

    array(
        'label'=> '',
        'desc'  => '',
        'id'    => $prefix.'ref',
        'type'  => 'editor',
                 'sanitizer' => 'wp_kses_post',
    'settings' => array(
        'textarea_name' =>  $prefix.'referencias'
    )
    )
    );

new Custom_Add_Meta_Box( 'shirley_headlines_box', __('Recipe Headline', __NAMESPACE__), $fields_headlines, 'shirley_recipes', true );
new Custom_Add_Meta_Box( 'shirley_teaser_box', __('Teaser', __NAMESPACE__), $fields_teaser, 'shirley_recipes', true );
new Custom_Add_Meta_Box( 'shirley_intro_box', __('Introduction', __NAMESPACE__), $fields_intro, 'shirley_recipes', true );
new Custom_Add_Meta_Box( 'shirley_facts_box', __('Facts', __NAMESPACE__), $fields_facts, 'shirley_recipes', true );
new Custom_Add_Meta_Box( 'shirley_nutrition_box', __('Nutrition', __NAMESPACE__), $fields_nutrition, 'shirley_recipes', true );
new Custom_Add_Meta_Box( 'shirley_ingredients_box', __('Ingredients', __NAMESPACE__), $fields_ingredients, 'shirley_recipes', true );
new Custom_Add_Meta_Box( 'shirley_cooking_box', __('Elaboración', __NAMESPACE__), $fields_cooking, 'shirley_recipes', true );
new Custom_Add_Meta_Box( 'shirley_notas_box', __('Notas', __NAMESPACE__), $fields_notas, 'shirley_recipes', true );
new Custom_Add_Meta_Box( 'shirley_ref_box', __('Referencias', __NAMESPACE__), $fields_ref, 'shirley_recipes', true );
