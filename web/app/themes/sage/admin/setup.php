<?php
// metaboxes directory constant
define( 'CUSTOM_METABOXES_DIR', get_template_directory_uri() . '/admin/metaboxes/metaboxes' );
//necessario para funcionar o metabox
add_action( 'admin_enqueue_scripts', function(){
    wp_enqueue_script( 'admin_scripts_js', get_template_directory_uri() . '/admin/metaboxes/js/scripts.js', false, '1.0.0' );
    wp_enqueue_script( 'admin_scripts_js', get_template_directory_uri() . '/admin/metaboxes/js/chosen.js', false, '1.0.0' );
});




///////////////
//custom ao admin
//////////////////
function change_footer_admin () {  
  echo 'Made  with ❤ by <a href="http://262media.com">262media</a>.';  
}  
  
add_filter('admin_footer_text', 'change_footer_admin');
add_filter('admin_title', 'my_admin_title', 10, 2);

function my_admin_title($admin_title, $title)
{
    return get_bloginfo('name').' &bull; '.$title;
}

/** changing default wordpres email settings */
 
add_filter('wp_mail_from', 'new_mail_from');
add_filter('wp_mail_from_name', 'new_mail_from_name');
 
function new_mail_from($old) {
 return 'apoio.cliente@262media.com';
}
function new_mail_from_name($old) {
 return 'Baked by Shirley website';
}
///////////////
//eof custom admin
///////////////
